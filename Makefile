.PHONY: init
init:
	git init
	git add .
	git remote add origin git@gitlab.com:timurkash-charts/find-psy.git
	git commit -m "Init"
	git push origin master
.PHONY: next
next:
	git add .
	git commit -m "next"
	git push -u origin master
.PHONY: package
packageall:
	helm package configmap nginx router sfpostgres stateless
	helm repo index ./ --url https://timurkash-charts.gitlab.io/helm/